<?php

/**
 * The administrator pages for the Webform Profile module.
 */
function webform_profile_admin_form(&$form_state) {
  
  // Get all of the user profile types.
  $types = content_profile_get_types();

  // Set the theme for this form.
  $form = array('#theme' => 'webform_profile_admin_form');
  
  // Store all of our field data.
  $fields = array();
   
  $enabled = array();
  $overridable = array();
  $autofill = array();
  
  // Iterate through our node types.
  foreach ($types as $node_type => $type) {
    
    // Iterate through the content type fields.
    foreach (webform_profile_get_fields($node_type) as $fieldid => $field) {
      
      // Add this to our fields array.
      $fields[$node_type . '-' . $fieldid] = $field;
      
      // Add our form options.
      $enabled[$node_type . '-' . $fieldid] = '';
      $overridable[$node_type . '-' . $fieldid] = '';
      $autofill[$node_type . '-' . $fieldid] = ''; 
    }
  }
  
  // Add the checkboxes for these submissions.
  $form['webform_profile_enabled'] = array('#type' => 'checkboxes', '#options' => $enabled, '#default_value' => variable_get('webform_profile_enabled', array_keys($enabled)));
  $form['webform_profile_overridable'] = array('#type' => 'checkboxes', '#options' => $overridable, '#default_value' => variable_get('webform_profile_overridable', array_keys($overridable)));
  $form['webform_profile_autofill'] = array('#type' => 'checkboxes', '#options' => $autofill, '#default_value' => variable_get('webform_profile_autofill', array_keys($autofill)));
  
  // Store the fields in this form.
  $form['#fields'] = array('#type' => 'value', '#value' => $fields);
  
  // Add a Save button.
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults') );
  
  // Add the typical system settings form submit handler.
  $form['#submit'][] = 'system_settings_form_submit';
  
  // Return the settings form.
  return $form;  
}

/**
 * Theme the webform_profile_admin_form
 */
function theme_webform_profile_admin_form($form) {
  $header = array('Field', 'Enabled', 'Overridable', 'AutoFill');
  $rows = array();
  
  $rows[] = array(
    '',
    'If checked, these profile fields will be enabled when creating webforms.',
    'If checked, these fields will be allowed to be overridden by the webform data.  Some data you may not want to allow the user to provide different data that is within their profile. ( such as Birthday ).',
    'If checked, these fields will autopopulate on the webform with the data within your profile.'
  );
  
  // Get the fields.
  $fields = $form['#fields']['#value'];
  
  // Iterate through all of our fields
  foreach ($fields as $fieldid => $field) {
    
    // Add this as a row showing the difference.
    $rows[] = array(
      $field['widget']['label'], 
      drupal_render($form['webform_profile_enabled'][$fieldid]),
      drupal_render($form['webform_profile_overridable'][$fieldid]),
      drupal_render($form['webform_profile_autofill'][$fieldid]),
    );
  }  
  
  // Generate the output.
  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;  
}